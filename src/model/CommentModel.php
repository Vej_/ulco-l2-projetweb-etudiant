<?php namespace model;


class CommentModel {

    public static function postComment( int $PID, int $UID, String $dat ): bool {
        if( !isset( $_SESSION[ "UID" ] ) ) return false;
        if( strlen( $_POST[ "comment_data" ] ) > 200 ) return false;

        $req= "INSERT INTO comment (content, id_product, id_account) VALUES ( \"" .$dat. "\", ".$PID.", " .$UID. ");";

        $DB= Model::connect();
        $res= $DB->prepare( $req );

        return $res->execute();
    }

    public static function listComments( int $PID ): array {
        $db= Model::connect();

        $fetch= "SELECT concat( U.firstname, ' ', U.lastname ) AS user, C.content AS comment_data, C.id_product FROM comment C
	                INNER JOIN account U ON U.id = C.id_account
                    HAVING C.id_product = " .$PID. ";";

        $res= $db->prepare( $fetch );
        $res->execute();

        return $res->fetchAll();
    }
}