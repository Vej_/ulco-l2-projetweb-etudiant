<?php namespace controller;

class ErrorController {
    public static function error(): void {
        $params= [
            "title" => "Erreur 404",
            "module" => "error.php"
        ];

        \view\Template::renderOnly($params);
    }
}