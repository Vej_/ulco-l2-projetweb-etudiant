<?php namespace controller;

use model\StoreModel;
use model\CommentModel;

class StoreController
{

    public function store(): void {
        // Communications avec la base de données
        $categories = StoreModel::listCategories();
        $products= StoreModel::listProducts();

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "products" => $products
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function product( int $id ) {

        $productinf= StoreModel::infoProduct( $id );
        $comments= CommentModel::listComments( $id );
        if( empty( $productinf ) ) {
            header( 'Location: /store', true );
            return;
        }

        $params= [
            "title" => $productinf[ "name" ],
            "module" => "product.php",
            "product" => $productinf,
            "comments" => $comments
        ];


        \view\Template::render($params);

    }

    public function search(): void {
        if( isset( $_POST[ "category" ] ) ) {
            foreach ($_POST["category"] as $k => $c) {
                $_POST[ "category" ][ $k ] = "'" . htmlspecialchars($c) . "'";
            }
        }

        if( isset( $_POST[ "sort" ] ) ) $_POST[ "sort" ]= htmlspecialchars( $_POST[ "sort" ] );

        $categories = StoreModel::listCategories();

        $products= StoreModel::searchProducts(
            htmlspecialchars(  $_POST[ "search" ] ),
            array(
                "category" => (isset( $_POST[ "category" ] )? $_POST[ "category" ] : null),
                "sort" => (isset( $_POST[ "sort" ] )? $_POST[ "sort" ] : null)
            )
        );

        if( $products == false ) { header( "Location: /store" ); exit(); }

        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "products" => $products
        );

        \view\Template::render($params);
    }

}