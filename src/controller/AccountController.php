<?php namespace controller;

use model\AccountModel;

class AccountController {

    public function account(): void {
        $params = array(
            "title" => "Account",
            "module" => "account.php"
        );

        \view\Template::render($params);
    }

    public function signin(): void {
        if( htmlspecialchars( $_POST[ 'userpass' ] ) === htmlspecialchars( $_POST[ 'uservpass' ] ) ) {
            $b = AccountModel::signin(
                htmlspecialchars($_POST['userfirstname']),
                htmlspecialchars($_POST['userlastname']),
                htmlspecialchars($_POST['usermail']),
                htmlspecialchars($_POST['userpass'])
            );
        }

        if( $b ) {
            header( "Location: /account?status=signin_success");
        } else {
            header( "Location: /account?status=signin_failed" );
        }

        exit();
    }

    public function login(): void {
        $b = AccountModel::login(
            htmlspecialchars( $_POST[ "usermail" ] ),
            htmlspecialchars( $_POST[ "userpass" ] )
        );

        if( $b == false ) {
            header( "Location: /account?status=login_failed", true );
        } else {
            header( "Location: /store", true );
        }

    }


    public function logout(): void {
        session_start();
        session_destroy();

        header( "Location: /account?status=logout", true );
        exit();
    }

    public function infos( int $UID ) {
        session_start();

        if( !isset( $_SESSION[ "UID" ] ) || $_SESSION[ "UID" ] != $UID ) {
            header( "Location: /store", true );
            exit();
        }

        $accFetch= AccountModel::retrieveAccountInfos( $UID );

        if( $accFetch != null ) {
            $params= [
                "title" => "Account infos",
                "module" => "infos.php",
                "firstname" => $accFetch[ "firstname" ],
                "lastname" => $accFetch[ "lastname" ],
                "email" => $accFetch[ "mail" ]
            ];

            \view\Template::render($params);

        } else ErrorController::error();

    }

    public static function update(): void {
        session_start();
        $b= AccountModel::update( array(
            "firstname" => htmlspecialchars( $_POST[ "fname" ] ),
            "lastname" => htmlspecialchars( $_POST[ "lname" ] ),
            "email" => htmlspecialchars( $_POST[ "mail" ] )
        ));

        if( $b ) {
            header( "Location: /account/".$_SESSION[ "UID" ]."?status=modify_success", true );
            $_SESSION[ "user" ]= htmlspecialchars( $_POST[ "fname" ] ) . " " . htmlspecialchars( $_POST[ "lname" ] );
            exit();
        } else {
            header( "Location: /account/".$_SESSION[ "UID" ]."?status=modify_fail", true );
            exit();
        }
    }
}