<?php
$product= $params[ "product" ];
$comments= $params[ "comments" ];
?>

<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $product[ "image" ]; ?>" >

            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $product[ "image" ] ?>" >
                </div>

                <div>
                    <img src="/public/images/<?= $product[ "image_alt1" ] ?>" >
                </div>

                <div>
                    <img src="/public/images/<?= $product[ "image_alt2" ] ?>" >
                </div>

                <div>
                    <img src="/public/images/<?= $product[ "image_alt3" ] ?>" >
                </div>
            </div>
        </div>

        <div class="product-infos">
            <p class="product-category"><?= $product[ "category" ]?></p>
            <h1><?=$product[ "name" ]?></h1>
            <p class="product-price"><?= $product[ "price" ]?>€</p>
            <form method="post" action="/cart/add" class="spaced">
                <div class="inline">
                    <button type="button">-</button>
                    <input type="number" name="quantity" value="1" id="quant" readonly/>
                    <button type="button">+</button>
                </div>

                <input type="hidden" value="<?=$product[ "id" ]?>" name="PID"/>
                <input type="submit">
            </form>
        </div>
    </div>

    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?= $product[ "spec" ] ?>
        </div>

        <form class="product-comments" action="/postComment" method="post">
            <h2>AVIS</h2>
            <?PHP
                if( empty( $comments ) ) {?>
                    <p>Pas d'avis sur ce produit..</p>
            <?PHP
                } else {
                    foreach( $comments as $c ) {?>
                        <div class="product-comment">
                            <p class="product-comment-author"><?=$c[ "user" ]?></p>
                            <p><?=$c[ "comment_data" ]?></p>
                        </div>
                    <?PHP
                    }
                }
                ?>
            <input type="text" name="comment_data" placeholder="Rediger un commentaire.." class="post-comment">
            <input type="hidden" name="PID" value="<?=$product[ "id" ]?>">
        </form>
    </div>
</div>

<script type="text/javascript" src="/public/scripts/product.js"></script>