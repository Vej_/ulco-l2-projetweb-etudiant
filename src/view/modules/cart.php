<div>
    <h1 class="cart-header">
        Votre panier...
    </h1>
</div>



    <?PHP
        if( empty( $_SESSION[ "cart_content" ] ) ) {?>
            <h3>Aucun objet..</h3>
        <?PHP
        } else {
            $content= $_SESSION[ "cart_content" ];
            ?>
            <form>
            <?php
            $totalPrice= 0;
            foreach( $content as $PID => $data ) {?>
                <div class="cart-element">
                    <div>
                        <img class="cart-img" src="/public/images/<?= $data[ "productinf" ][ "image" ] ?>" >
                    </div>

                    <div class="cart-contxt">

                        <div class="cart-inf">
                            <h4 class="cart-category"><?=$data[ "productinf" ][ "category" ]?></h4>
                            <h4><?=$data[ "productinf" ][ "name" ]?></h4>
                        </div>

                        <div class="cart-inf">
                            <h5>Quantité:</h5>
                            <div class="inline product-calc">
                                <button type="button" class="quant-specifier">-</button>
                                <input type="number" name="quantity" value="<?=$data["quantity"]?>" class="quant-specifier quantity" readonly/>
                                <button type="button" class="quant-specifier">+</button>

                                <input type="hidden" value="<?=$data[ "productinf" ][ "price" ]?>" class="price"/>
                            </div>
                        </div>

                        <div class="cart-inf centered">
                            <p>Prix à l'unité:</p>
                            <h4><?=$data[ "productinf" ][ "price" ]?> €</h4>
                        </div>
                    </div>
                </div>
            <?php
            $totalPrice += $data[ "productinf" ][ "price" ]* $data[ "quantity" ];
            }

            ?>
                <div class="cart-submit-container">
                    <button type="submit" class="btn-primary">Procéder au paiement..</button>
                    <pre>Prix total:<span id="total-price"><?=$totalPrice?> €</span></pre>
                </div>
            </form>
            <?php
}


    ?>
</div>

<script type="text/javascript" src="/public/scripts/cartProduct.js"></script>