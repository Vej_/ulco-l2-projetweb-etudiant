<?php
if( session_status() != PHP_SESSION_ACTIVE ) session_start();
?>

<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>

    <?php if( !isset( $_SESSION[ "UID" ] ) ) { ?>

        <a href="/account" class="account">
        <img src="/public/images/avatar.png">
        Compte
        </a>

    <?PHP } else { ?>

        <a href="/account/<?=$_SESSION[ "UID" ]?>" class="account">
            <img src="/public/images/avatar.png">
            <?= $_SESSION[ "user" ] ?>
        </a>
        <a href="/cart">Panier</a>
        <a href="/account/logout">Déconnexion</a>
    <?PHP } ?>
</nav>



