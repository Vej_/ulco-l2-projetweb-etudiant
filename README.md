# L2 Informatique - Projet Web

## Autheur
Leprêtre Hugo @Vej_

## Description

Développement d'une application simplifiée de e-commerce.

## Liens utiles

- [Sujet](https://florian-lepretre.herokuapp.com/teaching/projetweb/sujet/)
- [Dépôts des étudiants](https://docs.google.com/spreadsheets/d/16ydvylkxeVydqQASeoj1vYBUgb6zJ1EWZZEMHhHzSQ8/edit?usp=sharing)