<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

//* GET
$router->get( '/', 'controller\IndexController@index');
$router->get( '/store', 'controller\StoreController@store' );
$router->get( '/store/{:num}', 'controller\StoreController@product' );
$router->get( '/account', 'controller\AccountController@account' );
$router->get( '/account/logout', 'controller\AccountController@logout' );
$router->get( '/account/{:num}', 'controller\AccountController@infos' );
$router->get( '/cart', 'controller\CartController@get' );

//* POST
$router->post( '/account/login', 'controller\AccountController@login' );
$router->post( '/account/signin', 'controller\AccountController@signin' );
$router->post( '/postComment', 'controller\CommentController@postComment' );
$router->post( '/search', 'controller\StoreController@search' );
$router->post( '/account/update', 'controller\AccountController@update' );
$router->post( '/cart/add', 'controller\CartController@push' );

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
