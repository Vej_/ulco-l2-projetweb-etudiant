document.addEventListener( 'DOMContentLoaded', () => {

    document.getElementById( "lname" ).addEventListener( 'input', (e) => {
        let target= e.currentTarget

        if( target.value.length > 1 ) {
            target.classList.remove( 'invalid' );
            target.classList.add( 'valid' );
        } else {
            target.classList.add( 'invalid' );
            target.classList.remove( 'valid' );
        }


    });

    document.getElementById( "fname" ).addEventListener( 'input', (e) => {
        let target= e.currentTarget

        if( target.value.length > 1 ) {
            target.classList.remove( 'invalid' );
            target.classList.add( 'valid' );
        } else {
            target.classList.add( 'invalid' );
            target.classList.remove( 'valid' );
        }
    })

    document.getElementById( "mail" ).addEventListener( 'input', (e) => {
        let target = e.currentTarget

        // CREDITS: https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
        if( target.value.match( /^[^\s@]+@[^\s@]+$/ ) ) {
            target.classList.remove( 'invalid' );
            target.classList.add( 'valid' );
        } else {
            target.classList.add( 'invalid' );
            target.classList.remove( 'valid' );
        }
    });

    let el= document.getElementById( "pass" )

    if( el != null ) {  // pour module signin


        el.addEventListener('input', (e) => {
            let target = e.currentTarget;


            if (target.value.match(/(?=^(.{5,}[a-z]))(?=^(.*[0-9]))/)) {
                target.classList.remove('invalid');
                target.classList.add('valid');
            } else {
                target.classList.add('invalid');
                target.classList.remove('valid');
            }
        })

        document.getElementById("passv").addEventListener('input', (e) => {
            let target = e.currentTarget;

            if (e.currentTarget.value === document.getElementById('pass').value) {
                target.classList.remove('invalid');
                target.classList.add('valid');
            } else {
                target.classList.add('invalid');
                target.classList.remove('valid');
            }

        });

        document.getElementsByClassName("account-signin")[0].addEventListener('submit', (e) => {
            let childs = e.currentTarget.getElementsByTagName('input');
            for (let i = 0; i < childs.length - 1; i++) {
                if (!childs[i].classList.contains('valid')) e.preventDefault();
            }
        })



    } else { //pour modification des infos du compte


        document.getElementsByClassName( "account-modify" )[0].addEventListener('submit', (e) => {
            let childs = e.currentTarget.getElementsByTagName('input');

            for (let i = 0; i < childs.length - 1; i++) {
                if ( childs[i].classList.contains('invalid') ) e.preventDefault();
            }
        })


    }
});