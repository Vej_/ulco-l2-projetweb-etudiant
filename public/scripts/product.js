document.addEventListener( 'DOMContentLoaded', () => {
    const Form= document.getElementsByClassName( "product-infos" )[0]
                        .lastElementChild;
    const FormButtons= Form.getElementsByTagName( "button" );
    const quantity= document.getElementById( "quant" );
    const mainimg= document.getElementsByClassName( "product-images" )[0]
        .getElementsByTagName( "img" )[0];
    const miniatures= document.getElementsByClassName( "product-miniatures" )[0]
                               .getElementsByTagName( "div" );

    for( let divs of miniatures ) {
        img= divs.getElementsByTagName( "img" )[0];

        img.addEventListener( 'click', (ev) => {
            mainimg.setAttribute( "src", ev.currentTarget.getAttribute( "src" ) );
        })

    }

    function maxValueDiv( bool ) {
        if( bool ) {
            let e= document.createElement( "div" );
            e.classList.add( "box" ); e.classList.add( "error" );
            e.innerText= "Quantité maximale autorisée";

            Form.appendChild( e );
        } else {
            let e= Form.lastElementChild;
            e.parentElement.removeChild( e );
        }

    }

    FormButtons[0].addEventListener( 'click', () => {
        if( quantity.value > 1  ) {
            if( quantity.value == 5 ) maxValueDiv( false );
            quantity.value -= 1;
        }
    });

    FormButtons[1].addEventListener( 'click', () => {
        if( quantity.value < 5 ) {
            quantity.value -=- 1;
            if( quantity.value == 5 ) maxValueDiv( true );
        }
    });



})