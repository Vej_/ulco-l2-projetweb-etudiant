document.addEventListener( "DOMContentLoaded", function() {

    let quantities= document.getElementsByClassName( "product-calc" );
    let finalPrice= document.getElementById( "total-price" );

    function recalculatePrice() {
        let sum = 0;
        for (let item of quantities) {
            sum += item.getElementsByClassName("price")[0].value * item.getElementsByClassName("quantity")[0].value;
        }
        finalPrice.innerText= sum+' €';
    }

    for( let obj of quantities ) {



        let buttons= obj.getElementsByTagName( "button" );

        buttons[0].addEventListener( 'click', (e) => {
            let quantity= e.currentTarget.parentNode.getElementsByClassName( "quantity" )[0];
            if( quantity.value > 1 ) {
                quantity.value -= 1;
                recalculatePrice();
            }

        });

        buttons[1].addEventListener( 'click', (e) => {
            let quantity= e.currentTarget.parentNode.getElementsByClassName( "quantity" )[0];
            if( quantity.value < 5 ) {
                quantity.value -=- 1;
                recalculatePrice();
            }
        });
    }
})